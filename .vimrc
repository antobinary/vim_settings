
"#install pathogen
"mkdir -p ~/.vim/autoload ~/.vim/bundle && \
"curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
"#clone vim-coffee-script
"git clone https://github.com/kchmck/vim-coffee-script.git ~/.vim/bundle/vim-coffee-script/
"#enable vim-coffee-script in ~/.vimrc

"#https://github.com/tpope/vim-pathogen
"#https://github.com/kchmck/vim-coffee-script





call pathogen#infect()
syntax enable
"filetype plugin indent on

set shiftwidth=2
set softtabstop=2
set noai

" Highlight search results
set hlsearch

" Use spaces instead of tabs
set expandtab

"always display the filename
set modeline
set ls=2

" display the line number by default
set number

" map f7, f8 to navigate between tabs
" use gT or   map <F7> :tabp
" use gt or   map <F8> :tabn

" Return to last edit position when opening files
" autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif

" navigation amongst tabs
map <C-t><up> :tabr<cr>
map <C-t><down> :tabl<cr>
map <C-t><left> :tabp<cr>
map <C-t><right> :tabn<cr>
